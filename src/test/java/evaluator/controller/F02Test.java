package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

public class F02Test {
    private AppController appController;

    @Before
    public void setupController() {
        appController = new AppController();
    }

    @Test
    public void tc1() {
        try {
            this.appController.createNewTest();
            assert false;
        } catch (NotAbleToCreateTestException e) {
            assert true;
        }
    }

    @Test
    public void tc2() {
        try {
            this.appController.addNewIntrebare("Intrebare1?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu1");
            this.appController.addNewIntrebare("Intrebare2?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu1");
            this.appController.addNewIntrebare("Intrebare3?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu2");
            this.appController.addNewIntrebare("Intrebare4?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu2");
            this.appController.addNewIntrebare("Intrebare5?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu3");

            this.appController.createNewTest();
            assert false;
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            assert false;
        } catch (NotAbleToCreateTestException e) {
            assert true;
        }
    }

    @Test
    public void tc3() {
        try {
            this.appController.addNewIntrebare("Intrebare1?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu1");
            this.appController.addNewIntrebare("Intrebare2?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu2");
            this.appController.addNewIntrebare("Intrebare3?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu3");
            this.appController.addNewIntrebare("Intrebare4?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu4");
            this.appController.addNewIntrebare("Intrebare5?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu5");

            this.appController.createNewTest();
            assert this.appController.getAllTests().size() == 1;
        } catch (DuplicateIntrebareException | InputValidationFailedException | NotAbleToCreateTestException e) {
            assert false;
        }
    }

    @Test
    public void tc4() {
        try {
            this.appController.addNewIntrebare("Intrebare1?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu1");
            this.appController.addNewIntrebare("Intrebare2?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu2");
            this.appController.addNewIntrebare("Intrebare3?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu3");
            this.appController.addNewIntrebare("Intrebare4?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu4");
            this.appController.addNewIntrebare("Intrebare5?", "1) v1", "2) v2", "3) v3",
                    "1", "Domeniu5");

            this.appController.createNewTest();
            assert this.appController.getAllTests().size() == 1;
        } catch (DuplicateIntrebareException | InputValidationFailedException | NotAbleToCreateTestException e) {
            assert false;
        }
    }
}
